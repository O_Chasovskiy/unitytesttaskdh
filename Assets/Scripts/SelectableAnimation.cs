﻿using UnityEngine;

public class SelectableAnimation : MonoBehaviour, ISelectable
{
    private bool selected;
    private Vector3 initScale;

    private float maxScale = 1.2f;
    private float minScale = 0.8f;

    private void Start()
    {
        initScale = transform.localScale;
    }

    private void Update()
    {
        if (selected)
        {
            float scale = Mathf.PingPong(Time.time, maxScale - minScale) + minScale;
            transform.localScale = initScale * scale;
        }
    }

    public void Select()
    {
        if (!selected)
        {
            selected = true;
        }
    }

    public void DeSelect()
    {
        if (selected)
        {
            selected = false;
            transform.localScale = initScale;
        }
    }
}
