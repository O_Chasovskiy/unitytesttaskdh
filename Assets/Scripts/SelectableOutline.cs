﻿using UnityEngine;

public class SelectableOutline : MonoBehaviour, ISelectable
{
    private int initLayer;
    private bool selected;

    void Start()
    {
        initLayer = gameObject.layer;
    }

    public void Select()
    {
        if (!selected)
        {
            selected = true;
            gameObject.layer = 9;
        }
    }

    public void DeSelect()
    {
        if (selected)
        {
            selected = false;
            gameObject.layer = initLayer;
        }

    }
}
