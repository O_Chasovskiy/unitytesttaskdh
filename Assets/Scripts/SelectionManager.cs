﻿using UnityEngine;

public class SelectionManager : MonoBehaviour
{
    private ISelectable lastSelected;
    private ISelectable selected;

    private void Update()
    {
        if ( Input.GetAxis("Mouse X") != 0 ||  Input.GetAxis("Mouse Y") != 0)
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out var hit))
            {
                selected = hit.transform.GetComponent<ISelectable>();
                if (selected != null)
                {
                    lastSelected?.DeSelect();
                    lastSelected = selected;
                    selected.Select();
                }
                else
                {
                    lastSelected?.DeSelect();
                    lastSelected = selected = null;
                }
            }
            else if (selected != null)
            {
                selected.DeSelect();
                selected = null;
            }
        }

    }
}
