﻿using UnityEngine;

public class SelectableEmission : MonoBehaviour, ISelectable
{
    [SerializeField] private Color selectedColor = new Color(255f,0f,0f,0f);

    private int emissionID;
    private Renderer selectedRenderer;
    private bool selected;

    void Start()
    {
        emissionID = Shader.PropertyToID("_EmissionColor");
        selectedRenderer = GetComponent<Renderer>();
    }

    public void Select()
    {
        if (!selected)
        {
            selected = true;
            selectedRenderer.material.EnableKeyword("_EMISSION");
            selectedRenderer.material.SetColor(emissionID, selectedColor);
        }
    }

    public void DeSelect()
    {
        if (selected)
        {
            selected = false;
            selectedRenderer.material.DisableKeyword("_EMISSION");
        }
    }
}
